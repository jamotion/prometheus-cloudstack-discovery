Prometheus CloudStack Service Discovery
=======================================

`prometheus-cloudstack-discovery.py` generates target groups by querying the
CloudStack API. This is to be used in conjunction with Prometheus's
[custom service discovery][1] feature.

There are multiple tags available to configure service generation:

- **DisableMonitoring:** Add this Instance Tag with a value of '1', 'true', 'True' or 'TRUE' to 
your instance and this instance is ignored from monitoring. An existing target definition file
for this customer will be deleted (also for blackbox targets)  
- **DisableBlackbox:** Add this Instance Tag with a value of '1', 'true', 'True' or 'TRUE' to 
your instance and this instance is ignored from blackbox monitoring. An existing blackbox target 
definition file for this customer will be deleted.


Example
-------

```
$ prometheus-cloudstack-discovery.py --list
"Example Org": [
    {
      "labels": {
        "Customer": "Example Org",
        "name": "srv01",
        "zone": "ch-gva-2",
        "job": "cadvisor",
        "host": "Example Org",
      },
      "targets": [
        "185.19.31.167:8080"
      ]
    }
  ]
```

Using as Docker Container
-------------------------

### Simple example

```
docker run -v ./config:/config -v ./targets:/targets jamotion/prometheus-cloudstack-discovery
```

This command will run the docker container. The cloudstack.ini file has to be located at `./config`.
The container will generate multiple json files (one per instance)  in `.targets` folder. 
By default it does recheck the existing instances every 900 seconds (15 minutes).

As instance identifier a tag called *Customer* is used. You can change this key with command parameters.

### Complete example

```
docker run -e CLOUDSTACK_CONFIG=/targets/cloudstack.ini \
           -v ./config:/config \ 
           -v ./targets:/targets \
           jamotion/prometheus-cloudstack-discovery \
           --jobs "cadvisor:8080,nodeexporter:9190,ngix:9113" \
           --service \
           --sleep 900 \
           --host-tag Hostname \
           --dest /targets
```
This example shows the full parameter list (but without --list parameter). 
To check the functionality you can call the command with --list parameter. This will show the generated output instead
of writing it into files.

Install
-------

Make shure that `cs` python package is installed with:

`pip install cs`

Create a config file called `cloudstack.ini` with cloudstack connection information:

```
[cloudstack]
endpoint = https://api.exoscale.ch/compute
key = your-cs-key
secret = your-cs-secret
```

This file has to be located in same directory as the script is. Otherwise you can set the location of CloudStack
configuration file be setting the CLOUDSTACK_CONFIG environment variable.

Then configure a [file based service discovery] [1] target group in your `prometheus.yml` config:

```yaml
scrape_configs:
  - job_name: my_cloudstack_service

    file_sd_configs:
    - names: ['targets/*.json']
```

You can then setup a crontab job to update the target groups every minute:

```
* * * * * cd /path/to/prometheus/dir && prometheus-cloudstack-discovery.py
```

[1]: http://prometheus.io/blog/2015/06/01/advanced-service-discovery/#custom-service-discovery "Custom Service Discovery"