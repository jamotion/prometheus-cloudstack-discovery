FROM python:2-alpine
MAINTAINER Renzo Meister <rm@jamotion.ch>
RUN pip install cs

ADD prometheus-cloudstack-discovery.py /bin/prometheus-cloudstack-discovery.py
WORKDIR /config

VOLUME ["/config", "/targets"]

ENTRYPOINT ["python2", "/bin/prometheus-cloudstack-discovery.py"]

CMD ["--service", "--sleep=900"]