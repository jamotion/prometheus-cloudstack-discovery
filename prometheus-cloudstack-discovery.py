#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import time
import logging

_logger = logging.getLogger(__file__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

try:
    import json
except:
    import simplejson as json

try:
    from cs import CloudStack, CloudStackException, read_config
except ImportError:
    _logger.error("Error: CloudStack library must be installed: pip install cs.")
    sys.exit(1)


class CloudStackInventory(object):
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--jobs', default='cadvisor:8080')
        parser.add_argument('--list', action='store_true')
        parser.add_argument('--service', action='store_true')
        parser.add_argument('--dest', default='/targets')
        parser.add_argument('--blackbox', default='/targets/blackbox')
        parser.add_argument('--sleep', default='600')
        parser.add_argument('--host-tag', default='Customer')
        dir_path = os.path.dirname(os.path.realpath(__file__))
        config_file = os.path.join(dir_path, 'cloudstack.ini')
        if os.path.exists(config_file):
            os.environ['CLOUDSTACK_CONFIG'] = config_file

        options = parser.parse_args()

        if not options.list:
            if not os.path.exists(options.dest):
                os.mkdir(options.dest)
            if not os.path.exists(options.blackbox):
                os.mkdir(options.blackbox)

        try:
            self.cs = CloudStack(**read_config())
        except CloudStackException as e:
            _logger.error("Error: Could not connect to CloudStack API")
            sys.exit(1)

        self.read_services(options)

    def read_services(self, options):
        hosts = self.cs.listVirtualMachines()
        data = self.get_list(hosts, options)
        bb_data = self.get_blackbox_list(hosts, options)
        if options.list:
            _logger.info("Hostbased Targets")
            _logger.info(json.dumps(data, indent=2))
            _logger.info("Blackbox Targets")
            _logger.info(json.dumps(bb_data, indent=2))
        else:
            _logger.info("Updating services")
            for customer, instances in data.iteritems():
                target_file = os.path.join(options.dest,
                                           '{0}.json'.format(customer))
                if not instances:
                    if os.path.exists(target_file):
                        os.unlink(target_file)
                        _logger.info("Target {0} removed".format(customer))
                    continue

                new_json = json.dumps(instances, indent=2)
                if os.path.exists(target_file):
                    with open(target_file, 'r') as f:
                        old_json = f.read()
                        if new_json == old_json:
                            continue

                with open(target_file, 'w') as f:
                    _logger.info("Content of Instance {0} has changed".format(customer))
                    f.write(new_json)

            _logger.info("Updating blackbox services")
            for customer, instances in bb_data.iteritems():
                target_file = os.path.join(options.blackbox,
                                           '{0}.json'.format(customer))
                if not instances:
                    if os.path.exists(target_file):
                        os.unlink(target_file)
                        _logger.info("Blackbox Target {0} removed".format(customer))
                    continue

                new_json = json.dumps(instances, indent=2)
                if os.path.exists(target_file):
                    with open(target_file, 'r') as f:
                        old_json = f.read()
                        if new_json == old_json:
                            continue

                with open(target_file, 'w') as f:
                    _logger.info("Content of Blackbox Instance {0} has changed".format(customer))
                    f.write(new_json)

            if options.service:
                _logger.info("Waiting for {0} seconds".format(options.sleep))
                time.sleep(float(options.sleep))
                self.read_services(options)

    def get_list(self, hosts, options):
        data = {}
        if hosts:
            for host in hosts['virtualmachine']:
                customer = [c['value'] for c in host['tags'] if
                            c['key'] == options.host_tag]
                if not customer:
                    continue
                customer = customer[0]

                disable_monitoring = [c['value'] for c in host['tags'] if
                                      c['key'] == 'DisableMonitoring']
                if disable_monitoring and disable_monitoring[0] in ('1', 'true', 'True', 'TRUE'):
                    disable_monitoring = True
                else:
                    disable_monitoring = False

                if disable_monitoring:
                    data.setdefault(customer, False)
                    continue

                ip = [h['ipaddress'] for h in host['nic'] if h['isdefault']]
                if not ip:
                    continue
                ip = ip[0]

                tags = dict((t['key'], t['value']) for t in host['tags'])

                for job in options.jobs.split(','):
                    job_name, job_port = job.split(':')
                    instance = {
                        'targets': [
                            '{ip}:{port}'.format(ip=ip, port=job_port)],
                        'labels': {
                            'host': customer,
                            'job': job_name,
                            'name': host['displayname'],
                            'zone': host['zonename'],
                        }
                    }
                    instance['labels'].update(tags)
                    data.setdefault(customer, []).append(instance)
        return data

    def get_blackbox_list(self, hosts, options):
        data = {}
        if hosts:
            for host in hosts['virtualmachine']:
                customer = [c['value'] for c in host['tags'] if
                            c['key'] == options.host_tag]
                if not customer:
                    continue
                customer = customer[0]

                base_domain = [c['value'] for c in host['tags'] if
                               c['key'] == 'BaseDomain']
                if not base_domain:
                    continue
                base_domain = base_domain[0]

                disable_monitoring = [c['value'] for c in host['tags'] if
                                      c['key'] == 'DisableMonitoring']
                if disable_monitoring and disable_monitoring[0] in ('1', 'true', 'True', 'TRUE'):
                    disable_monitoring = True
                else:
                    disable_monitoring = False

                disable_blackbox = [c['value'] for c in host['tags'] if
                                    c['key'] == 'DisableBlackbox']
                if disable_blackbox and disable_blackbox[0] in ('1', 'true', 'True', 'TRUE'):
                    disable_blackbox = True
                else:
                    disable_blackbox = False

                if disable_blackbox or disable_monitoring:
                    data.setdefault(customer, False)
                    continue

                blackbox_url = [c['value'] for c in host['tags'] if
                                    c['key'] == 'BlackboxUrl']
                if blackbox_url:
                    blackbox_url = blackbox_url[0]
                else:
                    blackbox_url = '{0}.{1}'.format(customer, base_domain)

                tags = dict((t['key'], t['value']) for t in host['tags'])

                instance = {
                    'targets': [
                        'http://{0}/web/login'.format(blackbox_url)
                    ],
                    'labels': {
                        'host': customer,
                        'job': 'blackbox',
                        'url': blackbox_url,
                        'name': host['displayname'],
                        'zone': host['zonename'],
                    }
                }
                instance['labels'].update(tags)
                data.setdefault(customer, []).append(instance)
        return data


if __name__ == '__main__':
    CloudStackInventory()
